
// бургер -------------------------------------------
const menuBurger = document.querySelector('.burger-menu');
const headerContainer = document.querySelector('.top-header__container');
const headerBody = document.querySelector('.body-header');
const headerCatalog = document.querySelector('.catalog-header');
const body = document.querySelector('.body');

if(menuBurger) {
    // const headerNav = document.querySelector('.header__nav');
    menuBurger.addEventListener("click", function (params) {
        menuBurger.classList.toggle('_active');
        headerContainer.classList.toggle('_active');
        headerBody.classList.toggle('_active');
        headerCatalog.classList.toggle('_active');
        body.classList.toggle('_active');
    })
}


