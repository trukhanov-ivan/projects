// слайдеры ---------------------------------------

$(document).ready(function(){
   

    // if ( $(window).width() >= 769 ) {
        $('.main__partners-block').slick({
            arrows:true,
            dots:false,
            adaptiveHeight: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            rows: 1,
            responsive: [
                {
                breakpoint: 768,
                setting: {
                    arrows:false,
                    dots:true,
                    adaptiveHeight: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    slidesPerRow: 1,
                    rows: 2,
                    autoplay: false,
                    infinite: true,
                },
            }]
            
        });
        
        // $('.main__examples-block').slick({
        //     arrows:true,
        //     dots:true,
        //     adaptiveHeight: true,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     autoplay: false,
        // });

        // $('.reviews-body').slick({
        //     arrows:true,
        //     dots:true,
        //     adaptiveHeight: true,
        //     slidesToShow: 2,
        //     slidesToScroll: 1,
        //     autoplay: false,
        // });
    // }

    // if ( $(window).width() < 768 ) {
    //     $('.main__partners-block').slick({
    //         arrows:false,
    //         dots:true,
    //         adaptiveHeight: false,
    //         slidesToShow: 2,
    //         slidesToScroll: 1,
    //         slidesPerRow: 1,
    //         rows: 2,
    //         autoplay: false,
    //         infinite: true,
    //     });
    //     $('.main__advantages-block').slick({
    //         arrows:false,
    //         dots:true,
    //         adaptiveHeight: true,
    //         slidesToShow: 2,
    //         slidesToScroll: 2,
    //         slidesPerRow: 1,
    //         rows: 2,
    //         autoplay: false,
    //     });
    //     $('.services-container__block').slick({
    //         arrows:true,
    //         dots:false,
    //         adaptiveHeight: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         autoplay: false,
    //     });
    //     $('.main__examples-block').slick({
    //         arrows:true,
    //         dots:false,
    //         adaptiveHeight: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         autoplay: false,
    //     });
    //     $('.reviews-body').slick({
    //         arrows:false,
    //         dots:true,
    //         adaptiveHeight: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         slidesPerRow: 1,
    //         rows: 2,
    //     });
    // }
    // if ( $(window).width() < 576 ) {
        
    //     $('.questions-answers__block').slick({
    //         arrows:false,
    //         dots:true,
    //         adaptiveHeight: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         autoplay: false,
    //     });
    // }
    
});


// бургер -------------------------------------------
const menuBurger = document.querySelector('.header__burger');

if(menuBurger) {
    const headerNav = document.querySelector('.header__nav');
    let headerLogo = document.querySelector('.header__logo');
    menuBurger.addEventListener("click", function (params) {
        document.body.classList.toggle('_lock');
        menuBurger.classList.toggle('_active');
        headerNav.classList.toggle('_active');
        headerLogo.classList.toggle('_active');

    })
}

// хедер -------------------------------------------



const main = document.querySelector('.main');
const header = document.querySelector('.header');


const scrollListener = function (e) {

    if (scrollY > 70) {
        header.classList.add('_active-hed'); 
    } else {
        header.classList.remove('_active-hed');
    }

}

document.addEventListener('scroll', scrollListener);


