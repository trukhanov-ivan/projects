const menuIcon = document.querySelector(".header__icon-nav");
if (menuIcon) {
  const menuNav = document.querySelector(".header-nav");
  menuIcon.addEventListener("click", function (e) {
    document.body.classList.toggle("_lock");
    menuIcon.classList.toggle("_active");
    menuNav.classList.toggle("_active");
    let menuLang = document.querySelector(".home__lang");
    menuLang.classList.toggle("_active");
  });
}
