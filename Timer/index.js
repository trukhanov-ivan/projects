'use strict';
const inputBlock = document.querySelector('.input');
const titleDate = document.querySelector('#title-date');
const selectDateInput = document.querySelector('#date');
const buttonStart = document.querySelector('#btn');
const buttonReset = document.querySelector('#btn-reset');
const userTitle = document.querySelector('h1');
const countdownScreen = document.querySelector('.output');
let titleValue;
let dateValue;

buttonStart.addEventListener('click', startTimer);

// функция выбора даты
function selectDateFunc() {}

// функция счётчика
function countTimer() {}

// функция возобновления отсчёта после перезагрузки
function reloadTimer() {}

// функция начала отсчёта
function startTimer() {
    dateValue = selectDateInput.value;

    if (dateValue === '') {
        return alert('Пожалуйста введите дату');
    }

    titleValue = titleDate.value;
    userTitle.textContent = titleValue;
    countdownScreenFunc();
}

function countdownScreenFunc() {
    countdownScreen.classList.remove('hide');
    buttonReset.classList.remove('hide');
    inputBlock.classList.add('hide');
    buttonStart.classList.add('hide');
}
