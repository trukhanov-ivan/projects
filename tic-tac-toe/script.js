'use strict';

const cell = document.querySelectorAll('.cell');
const restartButton = document.querySelector('.game--restart');
const currentUserBlock = document.querySelector('.game--status');

let count = 0;
let currentUser = 'O';
let gameActive = true;
const gameState = ['', '', '', '', '', '', '', '', ''];
const winningLines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
];

function userMove(event) {
    const targetCellIndex = event.target.dataset.cellIndex;
    const isEmptyContent = event.target === undefined;
    const isEmptyTextContent = event.target.textContent === '';

    if (!gameActive || !isEmptyTextContent) {
        return;
    }

    currentUserBlock.textContent = `It's ${currentUser}'s turn`;
    currentUser = currentUser === 'O' ? 'X' : 'O';
    event.target.textContent = currentUser;
    gameState[targetCellIndex] = currentUser;

    console.log(gameState);
    gameActiveFunc();
}

function gameActiveFunc(event) {
    if (count === 8) {
        currentUserBlock.textContent = 'Game draw';
        gameActive = false;
        return;
    }

    for (let i = 0; i < winningLines.length; i++) {
        const winOption = winningLines[i];
        const a = gameState[winOption[0]];
        const b = gameState[winOption[1]];
        const c = gameState[winOption[2]];

        if (a === '' || b === '' || c === '') {
            continue;
        }
        if (a === b && b === c) {
            currentUserBlock.textContent = `Player ${currentUser}'s Win`;
            gameActive = false;
            break;
        }
    }
    count++;
}

function restartGameFunc() {
    for (let i = 0; i < cell.length; i++) {
        const isCellContent = cell[i];

        isCellContent.textContent = '';
    }

    gameState.splice(0, 9, '', '', '', '', '', '', '', '', '');
    currentUserBlock.textContent = `It's X's turn`;
    currentUser = 'O';
    count = 0;
    gameActive = true;
    console.log(gameState);
    return;
}

cell.forEach((elem) => elem.addEventListener('click', userMove));

restartButton.addEventListener('click', restartGameFunc);
